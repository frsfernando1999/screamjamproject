using System;
using System.Collections;
using UnityEngine;

public class BedInteraction : MonoBehaviour, IInteractable
{
    public event EventHandler OnSleptOnce;
    [SerializeField] private Dialogue dialogue;
    [SerializeField] private Dialogue inDreamDialogue;
    [SerializeField] private Dialogue sleepingDialogue;
    private bool _wantsToSleep = false;

    private bool _slept;
    private bool _canInteract = true;
    
    public string NotifyInteraction()
    {
        return "Examine";
    }

    public void Interact()
    {
        QuizUI.Instance.OnQuizSuccess += SetWantsToSleep;
        QuizUI.Instance.OnQuizFail += ResetInteract;
        if (_canInteract)
        {
            if (!_slept)
            {
                DialogueUI.Instance.SetInitialDialogue(dialogue);
                _canInteract = false;
            }
            else
            {
                DialogueUI.Instance.SetInitialDialogue(inDreamDialogue);
            }
        }
    }

    private void ResetInteract(object sender, EventArgs e)
    {
        QuizUI.Instance.OnQuizFail -= ResetInteract;
        QuizUI.Instance.OnQuizSuccess -= SetWantsToSleep;
        _canInteract = true;
    }

    private void SetWantsToSleep(object sender, EventArgs e)
    {
        QuizUI.Instance.OnQuizSuccess -= SetWantsToSleep;
        QuizUI.Instance.OnQuizFail -= ResetInteract;
        
        DialogueUI.Instance.OnFinishDialogue += Dream;
        _wantsToSleep = true;
    }

    private void Dream(object sender, EventArgs e)
    {
        DialogueUI.Instance.OnFinishDialogue -= Dream;
        FaderUI.Instance.FadeToBlack(Loader.Scene.None);
        MainPlayer.Instance.SetState(FirstPersonController.PlayerState.Occupied);
        StartCoroutine(DreamDialogue());
    }

    private IEnumerator DreamDialogue()
    {
        yield return new WaitForSeconds(1.0f);
        DialogueUI.Instance.SetInitialDialogue(sleepingDialogue);
        DialogueUI.Instance.OnFinishDialogue += WakeUp;
    }

    private void WakeUp(object sender, EventArgs e)
    {
        DialogueUI.Instance.OnFinishDialogue -= WakeUp;
        FaderUI.Instance.FadeClear();
        MainPlayer.Instance.SetState(FirstPersonController.PlayerState.Unoccupied);
        _slept = true;
        OnSleptOnce?.Invoke(this, EventArgs.Empty);
        GameManager.Instance.SetGhostsAlive(2);
        _canInteract = true;
    }
}