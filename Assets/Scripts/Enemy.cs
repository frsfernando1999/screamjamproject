using System;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private BedInteraction bedInteraction;
    [SerializeField] private float timeToDie = 3f;
    private float _timeTakingDamage = 0f;
    private bool _takingDamage = false;
    private bool _dead = false;
    private Animator _animator;
    private Collider _collider;
    private static readonly int Dying = Animator.StringToHash("Dying");
    private static readonly int TakingDamage = Animator.StringToHash("TakingDamage");

    private void Update()
    {
        if (_takingDamage)
        {
            _timeTakingDamage += Time.deltaTime;
        }
        if (_timeTakingDamage >= timeToDie)
        {
            if(_dead) return;
            _takingDamage = false;
            Die();
        }
    }

    private void Die()
    {
        _dead = true;
        GameManager.Instance.SetGhostsAlive(-1);
        _collider.enabled = false;
        _animator.SetTrigger(Dying);
    }

    private void Awake()
    {
        _collider = GetComponent<Collider>();
        _animator = GetComponent<Animator>();
    }

    private void OnEnable()
    {
        bedInteraction.OnSleptOnce += TurnZombiesOn;
        GameManager.Instance.OnAllGhostsDead += TurnZombiesOff;
    }

    private void TurnZombiesOff(object sender, EventArgs e)
    {
        gameObject.SetActive(false);
    }

    private void TurnZombiesOn(object sender, EventArgs e)
    {
        gameObject.SetActive(true);
    }

    private void Start()
    {
        gameObject.SetActive(false);
    }

    public void StartDeathTimer()
    {
        if (_dead) return;
        if (GameManager.Instance.readBook)
        {
            _animator.SetBool(TakingDamage, true);
            _takingDamage = true;
        }
    }

    public void ResetDeathTimer()
    {
        if (_dead) return;
        _animator.SetBool(TakingDamage, false);
        _takingDamage = false;
        _timeTakingDamage = 0.0f;
    }
    
}
