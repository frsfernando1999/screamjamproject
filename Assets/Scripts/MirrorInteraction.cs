using System;
using UnityEngine;

public class MirrorInteraction : MonoBehaviour, IInteractable
{
    [SerializeField] private Dialogue dialogue;
    [SerializeField] private Dialogue dialoguePostDream;
    [SerializeField] private OldMan oldMan;
    private DialogueUI _dialogueUI;
    private bool _slept;


    private void OnEnable()
    {
        oldMan.OnMirrorChangedDialogue += Slept;
    }

    private void Slept(object sender, EventArgs e)
    {
        oldMan.OnMirrorChangedDialogue -= Slept;
        _slept = true;
    }

    public string NotifyInteraction()
    {
        return "Examine";
    }

    public void Interact()
    {
        if (!_slept)
        {
            DialogueUI.Instance.SetInitialDialogue(dialogue);
        }
        else
        {
            DialogueUI.Instance.OnFinishDialogue += FinishGame;
            DialogueUI.Instance.SetInitialDialogue(dialoguePostDream); 
        }
    }

    private void FinishGame(object sender, EventArgs e)
    {
        DialogueUI.Instance.OnFinishDialogue -= FinishGame;
        Loader.Load(Loader.Scene.MainMenu);
    }
}
