using System;
using System.Collections;
using UnityEngine;

public class OldMan : MonoBehaviour, IInteractable
{
    public event EventHandler OnMirrorChangedDialogue;
    [SerializeField] private Dialogue dialogue;
    private void Start()
    {
        GameManager.Instance.OnAllGhostsDead += TurnOn;
        transform.parent.gameObject.SetActive(false);
    }

    private void TurnOn(object sender, EventArgs e)
    {
        GameManager.Instance.OnAllGhostsDead -= TurnOn;
        transform.parent.gameObject.SetActive(true);
    }

    public string NotifyInteraction()
    {
        return "Observe";
    }

    public void Interact()
    {
        DialogueUI.Instance.SetInitialDialogue(dialogue);
        DialogueUI.Instance.OnFinishDialogue += FinishGame;
    }

    private void FinishGame(object sender, EventArgs e)
    {
        FaderUI.Instance.FadeToBlack(Loader.Scene.None);
        MainPlayer.Instance.SetState(FirstPersonController.PlayerState.Occupied);
        StartCoroutine(WakeUp());
    }

    private IEnumerator WakeUp()
    {
        yield return new WaitForSeconds(1.0f);
        OnMirrorChangedDialogue?.Invoke(this, EventArgs.Empty);
        MainPlayer.Instance.SetInteractable(null);
        transform.parent.gameObject.SetActive(false);
        MainPlayer.Instance.SetState(FirstPersonController.PlayerState.Unoccupied);
        FaderUI.Instance.FadeClear();
        
    }
}
