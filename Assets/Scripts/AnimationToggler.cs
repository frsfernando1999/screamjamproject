using UnityEngine;

public class AnimationToggler : MonoBehaviour
{
    [SerializeField] private Animator animator;
    [SerializeField] private AudioSource footstep;
    [SerializeField] private CharacterController characterController;
    [SerializeField] private Inputs inputs;
    private static readonly int Speed = Animator.StringToHash("Speed");
    private static readonly int DirectionX = Animator.StringToHash("DirectionX");
    
    private void Update()
    {
        animator.SetFloat(DirectionX, inputs.move.y);
        animator.SetFloat(Speed, characterController.velocity.magnitude);
    }

    public void OnFootstep()
    {
        footstep.Play();
    }
}
