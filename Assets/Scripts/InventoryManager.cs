using System.Collections.Generic;
using UnityEngine;

public class InventoryManager : MonoBehaviour
{
    public static InventoryManager Instance;

    private List<InventoryItem> inventory = new List<InventoryItem>();

    public List<InventoryItem> Inventory => inventory;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void AddToInventory(InventoryItem item)
    {
        inventory.Add(item);
    }
}
