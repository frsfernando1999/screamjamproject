using UnityEngine;

public class Spawn : MonoBehaviour
{
    private FaderUI _faderUI;

    private void Awake()
    {
        _faderUI = GameObject.FindWithTag("Fader").GetComponent<FaderUI>();
    }
    private void Start()
    {
        if (!GameManager.Instance.firstSpawn)
        {
            GameObject player = GameObject.FindWithTag("Player");
            player.GetComponent<CharacterController>().enabled = false;
            player.GetComponent<FirstPersonController>().SetInteractable(null);
            player.transform.position = transform.position;
            player.transform.rotation = transform.localRotation;
            MainPlayer.Instance.SetState(FirstPersonController.PlayerState.Unoccupied);
            player.GetComponent<CharacterController>().enabled = true;
        }
        _faderUI.FadeClear();
    }
}
