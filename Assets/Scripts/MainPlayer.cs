using UnityEngine;

public class MainPlayer : MonoBehaviour
{
    [SerializeField] private FirstPersonController controller;
    [SerializeField] private Player playerStats;
    public static MainPlayer Instance;
    public Transform player;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void SetState(FirstPersonController.PlayerState newState)
    {
        controller.SetPlayerState(newState);
    }

    public FirstPersonController.PlayerState GetState()
    {
        return controller.GetPlayerState();
    }

    public void SetInteractable(IInteractable interactable)
    {
        controller.SetInteractable(interactable);
    }

    public void UpdateSanity(float value)
    {
        playerStats.UpdateSanity(value);
    }
}
