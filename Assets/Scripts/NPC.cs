using System;
using UnityEngine;

public class NPC : MonoBehaviour, IInteractable
{
    [SerializeField] private Dialogue dialogue;
    [SerializeField] private Dialogue dialogueHappy;
    [SerializeField] private Animator animator;
    [SerializeField] private InventoryItem key;
    private DialogueUI _dialogueUI;
    private static readonly int Talking = Animator.StringToHash("Talking");

    private void Awake()
    {
        _dialogueUI = GameObject.FindGameObjectWithTag("Dialogue").GetComponent<DialogueUI>();
    }
    
    private void ResetAnim(object sender, EventArgs e)
    {
        animator.SetBool(Talking, false);
        _dialogueUI.OnFinishDialogue -= ResetAnim;
    }


    public string NotifyInteraction()
    {
        return "Talk";
    }

    public void Interact()
    {
        var rotation = transform.rotation;
        transform.LookAt(GameObject.FindGameObjectWithTag("Player").transform);
        transform.rotation = new Quaternion(rotation.x, transform.rotation.y, rotation.z, transform.rotation.w);
        _dialogueUI.OnFinishDialogue += ResetAnim;
        if (InventoryManager.Instance.Inventory.Contains(key))
        {
            _dialogueUI.SetInitialDialogue(dialogueHappy);
            animator.SetBool(Talking, true);
            _dialogueUI.Show();
        }
        else
        { 
            _dialogueUI.SetInitialDialogue(dialogue);
            animator.SetBool(Talking, true);
            _dialogueUI.Show();
        }
    }
}
