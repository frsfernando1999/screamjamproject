using UnityEngine.SceneManagement;

public static class Loader {


    public enum Scene {
        MainMenu,
        EntryScene,
        LoadingScene,
        HouseScene,
        None
    }


    private static Scene targetScene;
    
    public static void Load(Scene target) {
        if (target == Scene.None) return;
        targetScene = target;
        SceneManager.LoadScene(Scene.LoadingScene.ToString());
    }

    public static void LoaderCallback() {
        SceneManager.LoadScene(targetScene.ToString());
    }

}