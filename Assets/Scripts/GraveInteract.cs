using UnityEngine;

public class GraveInteract : MonoBehaviour, IInteractable
{
    [SerializeField] private Dialogue dialogue;
    private DialogueUI _dialogueUI;

    private void Awake()
    {
        _dialogueUI = GameObject.FindWithTag("Dialogue").GetComponent<DialogueUI>();
    }

    public string NotifyInteraction()
    {
        return "Examine";
    }

    public void Interact()
    {
        _dialogueUI.SetInitialDialogue(dialogue);
    }
}
