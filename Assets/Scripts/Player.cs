using System;
using UnityEngine;

public class Player : MonoBehaviour
{
    public event EventHandler<OnSanityChangedArgs> OnSanityChanged; 
    [SerializeField] private Dialogue deathDialogue; 
    
    private float _sanity;
    private bool _dead;
    [SerializeField] private float maxSanity = 100.0f;


    private void Start()
    {
        _sanity = maxSanity;
        OnSanityChanged?.Invoke(this, new OnSanityChangedArgs(){Sanity = _sanity, MaxSanity = maxSanity});
    }

    public void UpdateSanity(float newValue)
    {
        _sanity = Mathf.Clamp(_sanity + newValue, 0, maxSanity);
        OnSanityChanged?.Invoke(this, new OnSanityChangedArgs(){Sanity = _sanity, MaxSanity = maxSanity});
        if (_sanity <= 0 && !_dead)
        {
            _dead = true;
            Die();
        }
    }

    private void Die()
    {
        MainPlayer.Instance.SetState(FirstPersonController.PlayerState.Occupied);
        FaderUI.Instance.FadeToBlack(Loader.Scene.None);
        DialogueUI.Instance.SetInitialDialogue(deathDialogue);
        DialogueUI.Instance.OnFinishDialogue += ResetGame;
    }

    private void ResetGame(object sender, EventArgs e)
    {
        Loader.Load(Loader.Scene.MainMenu);
    }
}

public class OnSanityChangedArgs : EventArgs
{
    public float Sanity;
    public float MaxSanity;
}
