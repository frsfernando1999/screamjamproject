using System;
using UnityEngine;

public class UVFlashlight : MonoBehaviour, IInteractable
{
    [SerializeField] private Dialogue dialogue;
    [SerializeField] private InventoryItem flashlightSO;
    private DialogueUI _dialogueUI;

    private void Awake()
    {
        _dialogueUI = DialogueUI.Instance;
    }


    public string NotifyInteraction()
    {
        return "Examine";
    }

    public void Interact()
    {
        _dialogueUI.OnFinishDialogue += Pickup;
        _dialogueUI.SetInitialDialogue(dialogue);
    }

    private void Pickup(object sender, EventArgs e)
    {
        InventoryManager.Instance.AddToInventory(flashlightSO);
        _dialogueUI.OnFinishDialogue -= Pickup;
        MainPlayer.Instance.SetInteractable(null);
        Destroy(transform.parent.gameObject);

    }
}
