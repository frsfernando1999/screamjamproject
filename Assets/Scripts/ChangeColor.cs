using System;
using UnityEngine;
using UnityEngine.Rendering;

public class ChangeColor : MonoBehaviour
{
    [SerializeField]private BedInteraction bedInteraction;
    private Volume _volume;

    private void Awake()
    {
        _volume = GetComponent<Volume>();
    }

    private void OnEnable()
    {
        GameManager.Instance.OnAllGhostsDead += TurnColorOff;
        bedInteraction.OnSleptOnce += TurnColorOn;
    }

    private void TurnColorOff(object sender, EventArgs e)
    {
        GameManager.Instance.OnAllGhostsDead -= TurnColorOff;
        _volume.profile.components[^1].active = false;
    }

    private void TurnColorOn(object sender, EventArgs e)
    {
        bedInteraction.OnSleptOnce -= TurnColorOn;
        _volume.profile.components[^1].active = true;
    }
}
