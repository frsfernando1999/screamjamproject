using UnityEngine;

public class Door : MonoBehaviour, IInteractable
{
    private FaderUI _faderUI;
    [SerializeField] private InventoryItem buildingKey; 

    private void Awake()
    {
        _faderUI = GameObject.FindWithTag("Fader").GetComponent<FaderUI>();
    }

    public string NotifyInteraction()
    {
        return "Open (Locked)";
    }

    public void Interact()
    {
        if (InventoryManager.Instance.Inventory.Contains(buildingKey))
        {
            MainPlayer.Instance.SetState(FirstPersonController.PlayerState.Interacting);
            GameManager.Instance.firstSpawn = false;
            _faderUI.LoadScene(Loader.Scene.HouseScene);
        }
    }
}
