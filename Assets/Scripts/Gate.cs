using System;
using UnityEngine;

public class Gate : MonoBehaviour, IInteractable
{
    [SerializeField] private Animator animator;
    [SerializeField] private InventoryItem key;
    [SerializeField] private Collider interactionCollider;
    private static readonly int Open = Animator.StringToHash("Open");
    private bool _open = false;

    public string NotifyInteraction()
    {
        return "Open (locked)";
    }

    public void Interact()
    {
        if (InventoryManager.Instance.Inventory.Contains(key) && !_open)
        {
            animator.SetTrigger(Open);
            interactionCollider.enabled = false;
            _open = true;
        }
    }
}
