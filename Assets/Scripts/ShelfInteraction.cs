using UnityEngine;

public class ShelfInteraction : MonoBehaviour, IInteractable
{
    [SerializeField] private Dialogue dialogue;
    private DialogueUI _dialogueUI;

    public string NotifyInteraction()
    {
        return "Examine";
    }

    public void Interact()
    {
        DialogueUI.Instance.SetInitialDialogue(dialogue);
    }
}
