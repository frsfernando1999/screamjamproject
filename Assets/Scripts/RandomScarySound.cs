using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class RandomScarySound : MonoBehaviour
{
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private List<AudioClip> audioClip = new List<AudioClip>();
    [SerializeField] private float minRandomTime = 5f;
    [SerializeField] private float maxRandomTime = 20f;
    
    private float _timePassed = 0.0f;
    private float _randomTime = 0.0f;
    private AudioClip _previousSound;

    private void Update()
    {
        _timePassed += Time.deltaTime;
        if (_timePassed >= _randomTime)
        {
            PlaySound();
        }
    }

    private void PlaySound()
    {
        _timePassed = 0.0f;
        _randomTime = Random.Range(minRandomTime, maxRandomTime);
        int randomSound = Random.Range(0, audioClip.Count);
        AudioClip audioToPlay = audioClip[randomSound];
        while (_previousSound == audioToPlay)
        {
            randomSound = Random.Range(0, audioClip.Count);
            audioToPlay = audioClip[randomSound];
        }
        _previousSound = audioToPlay;
        audioSource.clip = audioClip[randomSound];
        audioSource.Play();
    }

    private void Start()
    {
        _randomTime = Random.Range(minRandomTime, maxRandomTime);
    }
}
