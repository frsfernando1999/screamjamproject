using UnityEngine;

public class InteractionZone : MonoBehaviour
{
    [SerializeField] private GameObject interactable;
    private void OnTriggerEnter(Collider other)
    {
        FirstPersonController player = other.GetComponent<FirstPersonController>();
        if (player)
        {
            player.SetInteractable(interactable.GetComponent<IInteractable>());
        }
    }

    private void OnTriggerExit(Collider other)
    {
        FirstPersonController player = other.GetComponent<FirstPersonController>();
        if (player)
        {
            player.SetInteractable(null);
        }
    }
}
