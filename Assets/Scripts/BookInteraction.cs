using System;
using UnityEngine;

public class BookInteraction : MonoBehaviour, IInteractable
{
    [SerializeField] private Dialogue dialogue;
    [SerializeField] private bool important = false;
    [SerializeField] private BedInteraction bedInteraction;


    private void Start()
    {
        bedInteraction.OnSleptOnce += Show;
        GameManager.Instance.OnAllGhostsDead += Hide;
        transform.parent.gameObject.SetActive(false);
    }

    private void Hide(object sender, EventArgs e)
    {
        GameManager.Instance.OnAllGhostsDead -= Hide;
        transform.parent.gameObject.SetActive(false);
    }

    private void Show(object sender, EventArgs e)
    {
        bedInteraction.OnSleptOnce -= Show;
        transform.parent.gameObject.SetActive(true);
    }

    public string NotifyInteraction()
    {
        return "Read";
    }

    public void Interact()
    {
        DialogueUI.Instance.SetInitialDialogue(dialogue);
        if (important)
        {
            DialogueUI.Instance.OnFinishDialogue += MarkAsRead;
        }
    }

    private void MarkAsRead(object sender, EventArgs e)
    {
        GameManager.Instance.readBook = true;
    }
}
