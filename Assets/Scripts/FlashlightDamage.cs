using System;
using UnityEngine;

public class FlashlightDamage : MonoBehaviour
{
    private Enemy _enemy;
    private void OnTriggerEnter(Collider other)
    {
        _enemy = other.GetComponent<Enemy>();
        if (_enemy)
        {
            _enemy.StartDeathTimer();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        _enemy = other.GetComponent<Enemy>();
        if (_enemy)
        {
            _enemy.ResetDeathTimer();
            _enemy = null;
        }
    }

    private void OnDisable()
    {
        if (_enemy != null)
        {
            _enemy.ResetDeathTimer();
            _enemy = null;
        }
    }
}
