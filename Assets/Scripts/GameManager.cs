using System;
using System.Collections;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public event EventHandler OnAllGhostsDead;
    public InventoryItem ring;
    public static GameManager Instance;
    public Dialogue killAllGhostsDialogue;
    public bool firstSpawn = true;
    public int fishingCounter = 0;
    public int ghostsAlive;
    public bool readBook;
    public float ghostDamage = 1.0f;
    public float ringDef = 0.3f;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Update()
    {
        if (ghostsAlive > 0)
        {
            ApplySanityDamage();
        }
    }

    private void ApplySanityDamage()
    {
        float damage = ghostDamage;
        if (InventoryManager.Instance.Inventory.Contains(ring)) damage = ghostDamage - ringDef;
        float totalDamage = damage * ghostsAlive * Time.deltaTime;
        MainPlayer.Instance.UpdateSanity(-totalDamage);
        
    }


    public void SetGhostsAlive(int alive)
    {
        ghostsAlive += alive;
        if (ghostsAlive == 0)
        {
            FaderUI.Instance.FadeToBlack(Loader.Scene.None);
            DialogueUI.Instance.OnFinishDialogue += ChangeScreen;
            DialogueUI.Instance.SetInitialDialogue(killAllGhostsDialogue);
        }
    }

    private void ChangeScreen(object sender, EventArgs e)
    {
        MainPlayer.Instance.SetState(FirstPersonController.PlayerState.Occupied);
        StartCoroutine(ChangeRoom());
    }

    private IEnumerator ChangeRoom()
    {
        OnAllGhostsDead?.Invoke(this, EventArgs.Empty);
        yield return new WaitForSeconds(1.0f);    
        MainPlayer.Instance.SetState(FirstPersonController.PlayerState.Unoccupied);
        FaderUI.Instance.FadeClear();

    }
}
