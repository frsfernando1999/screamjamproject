using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Fish : MonoBehaviour, IInteractable
{
    [SerializeField] private FishingUI fishingUI;
    [SerializeField] private List<InventoryItem> trashList = new List<InventoryItem>();
    [SerializeField] private List<InventoryItem> usableList = new List<InventoryItem>();
    [SerializeField] private Dialogue trashDialogue;
    [SerializeField] private Dialogue usableDialogue;
    [SerializeField] private float fishTimerMin = 1.0f;
    [SerializeField] private float fishTimerMax = 3.0f;
    private DialogueUI _dialogueUI;
    private bool _canFish;

    private void Awake()
    {
        _dialogueUI = GameObject.FindGameObjectWithTag("Dialogue").GetComponent<DialogueUI>();
    }

    public string NotifyInteraction()
    {
        return "Fish";
    }

    public void Interact()
    {
        MainPlayer.Instance.SetState(FirstPersonController.PlayerState.Fishing);
        fishingUI.Show();
        GameManager.Instance.fishingCounter++;
        StartCoroutine(FishTimer());
    }

    private IEnumerator FishTimer()
    {
        yield return new WaitForSeconds(Random.Range(fishTimerMin, fishTimerMax));
        fishingUI.Hide();
        if (GetItemToReturn(out InventoryItem itemToReturn))
        {
            InventoryManager.Instance.AddToInventory(itemToReturn);
            usableDialogue.GetRootNode().SetText("You obtain a " + itemToReturn.itemName + ". It seems useful, you put it on your pocket.");
            _dialogueUI.SetInitialDialogue(usableDialogue);
        }
        else
        {
            trashDialogue.GetRootNode().SetText("You obtain a " + itemToReturn.itemName + ". It's trash, you throw it away.");
            _dialogueUI.SetInitialDialogue(trashDialogue);
        }
    }

    private bool GetItemToReturn(out InventoryItem itemToReturn)
    {
        int fishingCount = GameManager.Instance.fishingCounter;
        bool isUsable = true;
        if (fishingCount == 4)
        {
            itemToReturn = usableList[0];
        }
        else if (fishingCount == 2)
        {
            itemToReturn = usableList[1];
        }
        else if (fishingCount == 6)
        {
            itemToReturn = usableList[2];
        }
        else if (fishingCount == 10)
        {
            itemToReturn = usableList[3];
        }
        else
        {
            int randomNum = Random.Range(0, 4);
            itemToReturn = trashList[randomNum];
            isUsable = false;
        }
        return isUsable;
    }
}
