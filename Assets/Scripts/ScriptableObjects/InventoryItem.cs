using UnityEngine;

[CreateAssetMenu(fileName = "New Item", menuName = "Inventory Item", order = 0)]
public class InventoryItem : ScriptableObject
{
    public string itemName;
    public string description;
    public Sprite icon;
    public Sprite image;
}
