using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Dialogue Node", menuName = "Dialogue Node", order = 0)]
    public class DialogueNode : ScriptableObject
    {
        [SerializeField] private string speaker;
        [SerializeField] private string text;

        [SerializeField] private DialogueNode nextDialogueNode;
        [SerializeField] private Quiz quiz;
        public string GetSpeaker()
        {
            return speaker;
        }
        public Quiz GetQuiz()
        {
            return quiz;
        }
        public DialogueNode GetNextNode()
        {
            return nextDialogueNode;
        }
        
        public string GetText()
        {
            return text;
        }
        
        public bool HasQuiz()
        {
            return quiz != null;
        }

        public void SetText(string newText)
        {
            text = newText;
        }
    }
