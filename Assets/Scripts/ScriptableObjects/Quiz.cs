using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Quiz", menuName = "Quiz", order = 0)]
public class Quiz : ScriptableObject
{
    [SerializeField] private List<QuizQuestion> questionnaire = new List<QuizQuestion>();
    [SerializeField] private InventoryItem reward;
    private Dictionary<QuizQuestion, Dictionary<int, int>> _answerDictionary = new Dictionary<QuizQuestion, Dictionary<int, int>>();

    public Dictionary<QuizQuestion, Dictionary<int, int>> AnswerDictionary => _answerDictionary;
    public Dialogue passDialogue;
    public Dialogue failDialogue;
        
    [Range(0, 10)] public float passingGrade = 8;

    public InventoryItem Reward => reward;

    public void SetNewAnswer(QuizQuestion quizQuestion, int answer, int correctAnswer)
    {
        Dictionary<int, int> answers = new Dictionary<int, int> { { answer, correctAnswer } };
        _answerDictionary.Add(quizQuestion, answers);
    } 

    public QuizQuestion GetFirstQuestion()
    {
        return questionnaire[0];
    }
}
