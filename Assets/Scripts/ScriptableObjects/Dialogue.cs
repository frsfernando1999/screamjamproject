using System.Collections.Generic;
using System.Linq;
using UnityEngine;


    [CreateAssetMenu(fileName = "New Dialogue", menuName = "Dialogue", order = 0)]
    public class Dialogue : ScriptableObject
    {
        [SerializeField] private List<DialogueNode> nodes = new List<DialogueNode>();
        private Dictionary<string, DialogueNode> _nodeDictionary = new Dictionary<string, DialogueNode>();
        
        public IEnumerable<DialogueNode> GetAllNodes()
        {
            return nodes;
        }

        public DialogueNode GetRootNode()
        {
            if (nodes.Count == 0) return null;
            return nodes[0];
        }
        
        public DialogueNode GetNodeByID(string id)
        {
            var foundNode = nodes.FirstOrDefault(node => node.name == id);
            return foundNode;
        }
    }
