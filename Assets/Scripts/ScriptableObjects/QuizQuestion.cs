using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Quiz Question", menuName = "Quiz Question", order = 0)]

public class QuizQuestion : ScriptableObject
{
    [SerializeField] private string title;
    [SerializeField] private string question;
    [SerializeField] private List<string> options;
    [SerializeField] private int correctQuestion;
    [SerializeField] private QuizQuestion nextQuestion;
    
    public string Title => title;
    public string Question => question;
    public List<string> Options => options;
    public int CorrectQuestion => correctQuestion;
    public QuizQuestion NextQuestion => nextQuestion;
}
