using System;
using System.Collections;
using TMPro;
using UnityEngine;

public class DialogueUI : MonoBehaviour
{
    public static DialogueUI Instance;
    public event EventHandler OnFinishDialogue;
    [SerializeField] private TextMeshProUGUI speaker;
    [SerializeField] private TextMeshProUGUI dialogueText;
    [SerializeField] private float textSpeed = 0.01f;
    [SerializeField] private QuizUI quizUI;
    
    private bool _canPassText;
    private DialogueNode _currentNode;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(Instance.gameObject);
        }
    }

    void Start()
    {
        Hide();
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void SetInitialDialogue(Dialogue dialogue)
    {
        MainPlayer.Instance.SetState(FirstPersonController.PlayerState.InDialogue);
        Show();
        _currentNode = dialogue.GetRootNode();
        speaker.text = _currentNode.GetSpeaker();
        StartCoroutine(SetSlowText());
    }

    private IEnumerator SetSlowText()
    {
        string discourse = "";
        foreach (char letter in _currentNode.GetText())
        {
            discourse += letter;
            dialogueText.text = discourse;
            yield return new WaitForSeconds(textSpeed);
        }
        _canPassText = true;
    }

    public bool NextDialogueNode(FirstPersonController firstPersonController)
    {
        if (_currentNode.GetNextNode() != null && _canPassText)
        {
            _canPassText = false;
            _currentNode = _currentNode.GetNextNode();
            speaker.text = _currentNode.GetSpeaker();
            StartCoroutine(SetSlowText());
            return true;
        }

        if (_currentNode.HasQuiz())
        {
            quizUI.StartQuiz(_currentNode.GetQuiz());
            Hide();
            firstPersonController.SetPlayerState(FirstPersonController.PlayerState.OnQuiz);
            return true;
        }
        if (_currentNode.GetNextNode() == null && _canPassText && !_currentNode.HasQuiz())
        {
            Hide();
            _currentNode = null;
            _canPassText = false;
            OnFinishDialogue?.Invoke(this, EventArgs.Empty);
            return false;
        }
        return true;
    }
}
