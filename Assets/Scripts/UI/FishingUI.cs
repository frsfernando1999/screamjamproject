using System;
using UnityEngine;

public class FishingUI : MonoBehaviour
{
    private void Start()
    {
        Hide();
    }

    public void Show()
    {
        gameObject.SetActive(true);
    } 
    public void Hide()
    {
        gameObject.SetActive(false);
    } 
}
