using System;
using UnityEngine;

public class MainCanvasUI : MonoBehaviour
{
    public static MainCanvasUI Instance;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
