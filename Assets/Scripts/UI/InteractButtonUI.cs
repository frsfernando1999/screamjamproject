using System;
using TMPro;
using UnityEngine;

public class InteractButtonUI : MonoBehaviour
{
    [SerializeField] private FirstPersonController firstPersonController;
    [SerializeField] private TextMeshProUGUI message;
    
    private void Start()
    {
        firstPersonController.OnInteractionEnabled += Show;
        firstPersonController.OnInteractionDisabled += Hide;
        gameObject.SetActive(false);
    }

    private void Show(object sender, OnInteractionEnableArgs e)
    {
        message.text = e.UImessage;
        gameObject.SetActive(true);
    }

    public void Hide(object sender, EventArgs eventArgs)
    {
        gameObject.SetActive(false);
    }
}
