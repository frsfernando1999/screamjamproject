using System;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuUI : MonoBehaviour
{

    [SerializeField] private Button startButton;
    [SerializeField] private Button storyButton;
    [SerializeField] private StoryUI storyUI;

    private void Awake()
    {
        if (MainPlayer.Instance != null) Destroy(MainPlayer.Instance.gameObject);
        if (GameManager.Instance != null) Destroy(GameManager.Instance.gameObject);
        if (InventoryManager.Instance != null) Destroy(InventoryManager.Instance.gameObject);
        if (MainCanvasUI.Instance != null) Destroy(MainCanvasUI.Instance.gameObject);
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    private void Start()
    {
        startButton.onClick.AddListener(StartGame);
        storyButton.onClick.AddListener(() => { storyUI.Show(); });
    }

    private void StartGame()
    {
        Loader.Load(Loader.Scene.EntryScene);
    }
}
