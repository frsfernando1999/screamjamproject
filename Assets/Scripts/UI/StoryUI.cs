using UnityEngine;
using UnityEngine.UI;

public class StoryUI : MonoBehaviour
{
    [SerializeField] private GameObject story1;
    [SerializeField] private GameObject story2;
    [SerializeField] private GameObject story3;
    [SerializeField] private GameObject story4;
    
    [SerializeField] private Button story1Quit;
    [SerializeField] private Button story1Next;
    [SerializeField] private Button story2Previous;
    [SerializeField] private Button story2Next;
    [SerializeField] private Button story3Previous;
    [SerializeField] private Button story3Next;
    [SerializeField] private Button story4Previous;
    [SerializeField] private Button story4Close;
    
    private void Start()
    {
        ResetStory();
        BindButtons();
        Hide();
    }

    private void BindButtons()
    {
        story1Quit.onClick.AddListener(() =>
        {
            ResetStory();
            Hide();
        });
        story1Next.onClick.AddListener(() =>
        {
            story2.SetActive(true);
            story1.SetActive(false);
        });
        story2Previous.onClick.AddListener(() =>
        {
            story1.SetActive(true);
            story2.SetActive(false);
        });
        story2Next.onClick.AddListener(() =>
        {
            story3.SetActive(true);
            story2.SetActive(false);
        });
        story3Previous.onClick.AddListener(() =>
        {
            story2.SetActive(true);
            story3.SetActive(false);
        });
        story3Next.onClick.AddListener(() =>
        {
            story4.SetActive(true);
            story3.SetActive(false);
        });
        story4Previous.onClick.AddListener(() =>
        {
            story3.SetActive(true);
            story4.SetActive(false);
        });
        story4Close.onClick.AddListener(() =>
        {
            ResetStory();
            Hide();
        });
    }

    private void ResetStory()
    {
        story1.SetActive(true);
        story2.SetActive(false);
        story3.SetActive(false);
        story4.SetActive(false);
    }

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
}
