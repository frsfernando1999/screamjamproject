using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class QuizUI : MonoBehaviour
{
    public static QuizUI Instance;
    public event EventHandler OnQuizSuccess;
    public event EventHandler OnQuizFail;
    [SerializeField] private TextMeshProUGUI title;
    [SerializeField] private TextMeshProUGUI question;
    [SerializeField] private GameObject optionPrefab;
    [SerializeField] private Transform optionList;
    [SerializeField] private float textSpeed = 0.01f;
    [SerializeField] private float questionSpeed = 0.25f;
    private bool canAnswer = false;
    private Quiz _currentQuiz;
    private QuizQuestion _currentQuestion;
    private DialogueUI _dialogueUI;
    
    void Start()
    {
        Hide();
    }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(Instance.gameObject);
        }
        _dialogueUI = GameObject.FindGameObjectWithTag("Dialogue").GetComponent<DialogueUI>();

    }

    public void Show()
    {
        gameObject.SetActive(true);
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    public void StartQuiz(Quiz quiz)
    {
        Show();
        _currentQuiz = quiz;
        _currentQuestion = _currentQuiz.GetFirstQuestion();
        StartCoroutine(WriteQuestion());
    }

    private IEnumerator WriteQuestion()
    {
        title.text = _currentQuestion.Title;
        string questionText = "";
        foreach (Transform questionOption in optionList)
        {
            Destroy(questionOption.gameObject);
        }
        
        foreach (char letter in _currentQuestion.Question)
        {
            questionText += letter;
            question.text = questionText;
            yield return new WaitForSeconds(textSpeed);
        }

        foreach (string questionOption in _currentQuestion.Options)
        {
            yield return new WaitForSeconds(questionSpeed);
            GameObject option = Instantiate(optionPrefab, optionList);
            option.GetComponent<Button>().onClick.AddListener(() =>
            {
                if (canAnswer)
                {
                    _currentQuiz.SetNewAnswer(_currentQuestion, option.transform.GetSiblingIndex(), _currentQuestion.CorrectQuestion);
                    canAnswer = false;
                    NextQuestion();
                }
            });
            TextMeshProUGUI answerText = option.GetComponentInChildren<TextMeshProUGUI>();
            answerText.text = questionOption;
        }
        canAnswer = true;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }

    private void NextQuestion()
    {
        if (_currentQuestion.NextQuestion == null)
        {
            if (Evaluate())
            {
                _dialogueUI.SetInitialDialogue(_currentQuiz.passDialogue);
            }
            else
            {
                _dialogueUI.SetInitialDialogue(_currentQuiz.failDialogue);
            }
            Hide();
        }
        else
        {
            _currentQuestion = _currentQuestion.NextQuestion;
            StartCoroutine(WriteQuestion());
        }
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }
    private bool Evaluate()
    {
        float score = 0;
        foreach (KeyValuePair<QuizQuestion,Dictionary<int,int>> option in _currentQuiz.AnswerDictionary)
        {
            foreach (KeyValuePair<int,int> answers in option.Value)
            {
                if (answers.Value == answers.Key)
                {
                    score++;
                }
            }
        }
        float finalScore = score / _currentQuiz.AnswerDictionary.Count * 10;
        _currentQuiz.AnswerDictionary.Clear();
        if (finalScore >= _currentQuiz.passingGrade)
        {
            if (_currentQuiz.Reward != null)
            {
                InventoryManager.Instance.AddToInventory(_currentQuiz.Reward);
            }
            OnQuizSuccess?.Invoke(this, EventArgs.Empty);
            return true;
        }
        OnQuizFail?.Invoke(this, EventArgs.Empty);
        return false;
    }

}
