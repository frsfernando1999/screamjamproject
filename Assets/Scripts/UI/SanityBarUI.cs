using UnityEngine;
using UnityEngine.UI;

public class SanityBarUI : MonoBehaviour
{
    [SerializeField] private Player player;
    [SerializeField] private Image bar;
    
    
    private void OnEnable()
    {
        if (player == null)
        {
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<Player>();
        }
        player.OnSanityChanged += UpdateBar;
    }

    private void UpdateBar(object sender, OnSanityChangedArgs e)
    {
        bar.transform.localScale = new Vector3(e.Sanity / e.MaxSanity, 1, 1);
    }
}
