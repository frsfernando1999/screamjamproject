using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class FaderUI : MonoBehaviour
{
    public static FaderUI Instance;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(Instance.gameObject);
        }
    }

    [SerializeField] private Image blackImage;
    [SerializeField] private float fadeSpeed = 1.0f;

    private IEnumerator _fadeRoutine;
    
    private IEnumerator FadeRoutine(float targetAlpha, Loader.Scene sceneToLoad)
    {
        while (!Mathf.Approximately(blackImage.color.a, targetAlpha))
        {
            float alpha = Mathf.MoveTowards(blackImage.color.a, targetAlpha, fadeSpeed * Time.deltaTime);
            blackImage.color = new Color(blackImage.color.r, blackImage.color.g, blackImage.color.b, alpha);
            yield return null;
        }

        Finished(targetAlpha, sceneToLoad);
    }

    private void Finished(float targetAlpha, Loader.Scene sceneToLoad)
    {
        if (targetAlpha == 1)
        {
            Loader.Load(sceneToLoad);
        }
    }

    public void FadeToBlack(Loader.Scene sceneToLoad)
    {
        if (_fadeRoutine != null)
        {
            StopCoroutine(_fadeRoutine);
        }
        _fadeRoutine = FadeRoutine(1f, sceneToLoad);
        StartCoroutine(_fadeRoutine);
    }

    public void FadeClear()
    {
        if (_fadeRoutine != null)
        {
            StopCoroutine(_fadeRoutine);
        }
        _fadeRoutine = FadeRoutine(0f, Loader.Scene.None);
        StartCoroutine(_fadeRoutine);

    }



    public void LoadScene(Loader.Scene sceneToLoad)
    {
        FadeToBlack(sceneToLoad);
    }
}
