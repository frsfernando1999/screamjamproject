using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InventoryUI : MonoBehaviour
{
    [SerializeField] private GameObject itemButton;
    [SerializeField] private Transform itemListRoot;
    [SerializeField] private Image itemImage;
    [SerializeField] private TextMeshProUGUI description;
    [SerializeField] private InventoryItem goku;
    [SerializeField] private AudioSource audioSrc;
    private FirstPersonController _controller;
    private Inputs _input;

    private void Awake()
    {
        _controller = GameObject.FindGameObjectWithTag("Player").GetComponent<FirstPersonController>();
        _input = GameObject.FindGameObjectWithTag("Player").GetComponent<Inputs>();
    }

    private void OpenInventory(object sender, EventArgs e)
    {
        FirstPersonController.PlayerState state = MainPlayer.Instance.GetState();
        if (state == FirstPersonController.PlayerState.Unoccupied)
        {
            MainPlayer.Instance.SetState(FirstPersonController.PlayerState.BrowsingInventory);
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            Show();
            UpdateItemList();
        }
        else if (state == FirstPersonController.PlayerState.BrowsingInventory)
        {
            MainPlayer.Instance.SetState(FirstPersonController.PlayerState.Unoccupied);
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
            Hide();
        }
        else
        {
            HideUI();
        }
    }

    void Start()
    {
        _input.OnOpenInventoryPressed += OpenInventory;
        _controller.OnPlayerStateChanged += HideInventory;
        Hide();
    }

    private void HideInventory(object sender, FirstPersonController.PlayerStateChangedArgs e)
    {
        if (MainPlayer.Instance.GetState() != FirstPersonController.PlayerState.BrowsingInventory)
        {
            HideUI();
        }
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }

    public void Show()
    {
        gameObject.SetActive(true);
        UpdateItemList();
    }

    private void UpdateItemList()
    {
        foreach (Transform item in itemListRoot)
        {
            Destroy(item.gameObject);
        }

        foreach (InventoryItem item in InventoryManager.Instance.Inventory)
        {
            ItemButton button = Instantiate(itemButton, itemListRoot).GetComponent<ItemButton>();
            button.text.text = item.itemName;
            button.buttonIcon.sprite = item.icon;
            button.GetComponent<Button>().onClick.AddListener(() =>
            {
                itemImage.sprite = item.image;
                description.text = item.description;
                if (item == goku)
                {
                    audioSrc.Play();
                    StartCoroutine(HideUIGoku());
                }
            });
        }
    }

    private IEnumerator HideUIGoku()
    {
        yield return new WaitForSeconds(0.5f);
        MainPlayer.Instance.SetState(FirstPersonController.PlayerState.Unoccupied);
        HideUI();
    }

    private void HideUI()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        Hide();
    }
}