public interface IInteractable
{
    public string NotifyInteraction();
    public void Interact();
}
